#!/usr/bin/env python

import rospy
from ackermann_publisher import AckermannPublisher
import math

FIXED_ANGLE_DEGREES = 30
FIXED_ANGLE_RADIANS = math.radians(FIXED_ANGLE_DEGREES)

COS_FIXED_ANGLE = math.cos(FIXED_ANGLE_RADIANS)
SIN_FIXED_ANGLE = math.sin(FIXED_ANGLE_RADIANS)


class SteeringChecking(AckermannPublisher):
    def __init__(self, node_name):
        super(SteeringChecking, self).__init__(node_name)
        self.direction = rospy.get_param("~direction", "left")
        r = rospy.Rate(50)
        while not rospy.is_shutdown():
            self.steer()
            r.sleep()

    def steer(self):
        angle = FIXED_ANGLE_RADIANS
        if self.direction == "right":
            angle *= -1.0
        self.publish_ackermann(angle, 0)


if __name__ == '__main__':
    SteeringChecking("steering_checker")