#!/usr/bin/env python

import rospy
import csv
from geometry_msgs.msg import PoseWithCovarianceStamped


class WaypointLogger(object):
    def __init__(self):
        rospy.init_node("waypoint_logger")
        rospy.loginfo("waypoint_logger node started")

        self.csv_file = open('waypoints.csv', 'wb')
        self.csv_writer = csv.writer(self.csv_file)
        rospy.on_shutdown(self.on_shutdown)

        self.previous_point = None

        rospy.Subscriber("/amcl_pose", PoseWithCovarianceStamped, self.record_waypoint,
                         queue_size=5)

    def record_waypoint(self, data):
        wayp = data.pose.pose.position
        if self.previous_point:
            if (wayp.x - self.previous_point.x)**2 + (wayp.y - self.previous_point.y)**2 > 0.25:
                # only record waypoints if they are atleast 0.5m apart
                self.csv_writer.writerow([wayp.x, wayp.y])
                self.previous_point = wayp
        else:
            self.csv_writer.writerow([wayp.x, wayp.y])
            self.previous_point = wayp

    def on_shutdown(self):
        # close the csv file before exiting
        rospy.loginfo("Closing csv file")
        self.csv_file.close()


if __name__ == "__main__":
    logger = WaypointLogger()

    # this line simply prevents the Python Main thread from exiting
    rospy.spin()