#!/usr/bin/env python

import rospy
import os
import csv
from actionlib_msgs.msg import GoalStatus
from geometry_msgs.msg import PoseStamped


class GoalPublisher(object):
    def __init__(self):
        rospy.init_node("goal_publisher")
        rospy.loginfo("waypoint_logger node started")

        rospy.loginfo("Current directory: %s" % os.getcwd())
        
        file_name = rospy.get_param("~waypoint_file", "waypoints/waypoints_home.csv")
        rospy.loginfo("Waypoint file %s" % file_name)

        self.csv_file = open(file_name, 'rb')
        self.csv_reader = csv.reader(self.csv_file)
        self.waypoint_gen = self.waypoint_generator()

        rospy.on_shutdown(self.on_shutdown)

        self.previous_point = None

        rospy.Subscriber("/move_base/status", GoalStatus, self.check_status,
                         queue_size=5)

        self.goal_publisher = rospy.Publisher("/move_base_simple/goal", PoseStamped, queue_size=5)
        # wait for the publisher to be ready
        rospy.sleep(1)

        # publish first goal
        self.load_next_waypoint()

    def check_status(self, data):
        if data.status == GoalStatus.SUCCEEDED:
            self.load_next_waypoint()

    def load_next_waypoint(self):
        try:
            rospy.loginfo("loading next waypoint")
            wayp = next(self.waypoint_gen)
            self.publish_goal(wayp)
        except StopIteration:
            rospy.logwarn("No more waypoints in the CSV file")

    def waypoint_generator(self):
        for row in self.csv_reader:
            yield float(row[0]), float(row[1])

    def publish_goal(self, wayp):
        goal = PoseStamped()

        goal.header.stamp = rospy.Time.now()
        goal.header.frame_id = "map"

        goal.pose.position.x = wayp[0]
        goal.pose.position.y = wayp[1]
        goal.pose.position.z = 0.0

        goal.pose.orientation.x = 0.0
        goal.pose.orientation.y = 0.0
        goal.pose.orientation.z = 0.0
        goal.pose.orientation.w = 1.0

        self.goal_publisher.publish(goal)

    def on_shutdown(self):
        # close the csv file before exiting
        rospy.loginfo("Closing csv file")
        self.csv_file.close()


if __name__ == "__main__":
    logger = GoalPublisher()

    # this line simply prevents the Python Main thread from exiting
    rospy.spin()