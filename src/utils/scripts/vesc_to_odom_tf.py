#!/usr/bin/env python

import rospy
from geometry_msgs.msg import TransformStamped, Quaternion
from nav_msgs.msg import Odometry
import tf2_ros
import tf


class VescToOdom(object):
    def __init__(self):
        rospy.init_node("vesc_to_odom_tf", log_level=rospy.INFO)

        self.tf_broadcaster = tf2_ros.TransformBroadcaster()
        rospy.Subscriber("/vesc/odom", Odometry, self.callback)

        rospy.loginfo("Started node %s" % "vesc_to_odom_tf")

    def callback(self, data):
        msg = TransformStamped()
        msg.header.stamp = data.header.stamp
        msg.header.frame_id = "odom"
        msg.child_frame_id = "base_footprint"

        msg.transform.translation.x = data.pose.pose.position.x
        msg.transform.translation.y = data.pose.pose.position.y
        msg.transform.translation.z = 0.0
        msg.transform.rotation = data.pose.pose.orientation

        self.tf_broadcaster.sendTransform(msg)


if __name__ == '__main__':
    VescToOdom()
    rospy.spin()