#!/usr/bin/env python

from visualization_msgs.msg import Marker
import rospy


class MarkerDisplay(object):
    def __init__(self):
        rospy.init_node("visualize_marker", log_level=rospy.INFO)

        self.marker_pub = rospy.Publisher("/visualization_marker", Marker, queue_size=5)
        rospy.loginfo("Started node visualize_marker")

        r = rospy.Rate(10)
        while not rospy.is_shutdown():
            self.publish_marker()
            r.sleep()

    def publish_marker(self):
        marker = Marker()

        marker.header.frame_id = "map"
        marker.header.stamp = rospy.Time.now()

        marker.ns = "basic_shapes"
        marker.id = 0


        marker.type = Marker.ARROW
        marker.action = Marker.ADD

        marker.pose.position.x = 0
        marker.pose.position.y = 0
        marker.pose.position.z = 0
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = 0.0
        marker.pose.orientation.w = 1.0

        marker.scale.x = 1.0
        marker.scale.y = 0.1
        marker.scale.z = 0.1

        marker.color.r = 0.0
        marker.color.g = 1.0
        marker.color.b = 0.0
        marker.color.a = 1.0

        marker.lifetime = rospy.Duration(0.1)

        self.marker_pub.publish(marker)

        marker.id = 1
        marker.pose.position.x = 1
        marker.pose.position.y = 0
        self.marker_pub.publish(marker)

        marker.id = 2
        marker.pose.position.x = 3
        marker.pose.position.y = 2
        self.marker_pub.publish(marker)


if __name__ == '__main__':
    MarkerDisplay()
    # rospy.spin()