#!/usr/bin/env python

import rospy
import os
import csv
from geometry_msgs.msg import PoseStamped


class WaypointRecorder(object):
    def __init__(self):
        rospy.init_node("waypoint_recorder")
        rospy.loginfo("waypoint_recorder node started")

        rospy.loginfo("Current directory: %s" % os.getcwd())

        file_name = rospy.get_param("~waypoint_file", "waypoints/waypoints_home.csv")
        rospy.loginfo("Waypoint file %s" % file_name)

        self.csv_file = open(file_name, 'wb')
        self.csv_writer = csv.writer(self.csv_file)

        rospy.on_shutdown(self.on_shutdown)

        rospy.Subscriber("/move_base_simple/goal", PoseStamped, self.record_waypoint,
                         queue_size=5)

    def record_waypoint(self, msg):
        x = "%.2f" % msg.pose.position.x
        y = "%.2f" % msg.pose.position.y
        qz = "%.2f" % msg.pose.orientation.z
        qw = "%.2f" % msg.pose.orientation.w
        self.csv_writer.writerow([x, y, qz, qw])

    def on_shutdown(self):
        # close the csv file before exiting
        rospy.loginfo("Closing csv file")
        self.csv_file.close()


if __name__ == "__main__":
    WaypointRecorder()
    # this line simply prevents the Python Main thread from exiting
    rospy.spin()