#!/usr/bin/env python

import rospy
import os
import csv
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import String


class GoalPublisher(object):
    def __init__(self):
        rospy.init_node("goal_publisher")
        rospy.loginfo("waypoint_loader node started")
        self.file_name = rospy.get_param("~waypoint_file", "waypoints/waypoints_home.csv")
        self.is_cyclic = rospy.get_param("~cyclic", False)

        rospy.loginfo("Current directory: %s" % os.getcwd())
        rospy.loginfo("Waypoint file %s" % self.file_name)
        self.waypoints = self.waypoint_loader()

        self.waypoint_index = 0

        self.goal_publisher = rospy.Publisher("/move_base_simple/goal", PoseStamped, queue_size=5)
        rospy.Subscriber("/goal_status", String, self.publish_next_waypoint, queue_size=5)
        # wait for the publisher to be ready
        rospy.sleep(1)

        # publish first goal
        self.publish_next_waypoint("next")

    def get_next_goal(self):
        rospy.loginfo("loading next waypoint")
        if self.waypoint_index == len(self.waypoints):
            if self.is_cyclic:
                self.waypoint_index = self.waypoint_index % len(self.waypoints)
            else:
                rospy.logwarn("Waypoints exhausted! Non cyclic waypoint server")
                return None

        wayp = self.waypoints[self.waypoint_index]
        self.waypoint_index += 1

        goal = PoseStamped()

        goal.header.stamp = rospy.Time.now()
        goal.header.frame_id = "map"

        goal.pose.position.x = wayp[0]
        goal.pose.position.y = wayp[1]
        goal.pose.position.z = 0.0

        goal.pose.orientation.x = 0.0
        goal.pose.orientation.y = 0.0
        goal.pose.orientation.z = wayp[2]
        goal.pose.orientation.w = wayp[3]

        return goal

    def publish_next_waypoint(self, msg):
        goal = self.get_next_goal()
        if goal:
            self.goal_publisher.publish(goal)

    def waypoint_loader(self):
        waypoints = []
        with open(self.file_name, 'rb') as csv_file:
            csv_reader = csv.reader(csv_file)
            for row in csv_reader:
                waypoints.append([float(row[0]), float(row[1]), float(row[2]), float(row[3])])
        return waypoints



if __name__ == "__main__":
    publisher = GoalPublisher()

    # this line simply prevents the Python Main thread from exiting
    rospy.spin()