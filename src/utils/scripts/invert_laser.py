#!/usr/bin/env python

from sensor_msgs.msg import LaserScan
import rospy


class InvertLaser(object):
    def __init__(self):
        rospy.init_node("invert_laser", log_level=rospy.INFO)

        self.scan_publisher = rospy.Publisher("/inverted_scan", LaserScan, queue_size=5)
        rospy.Subscriber("/inverted_scan", LaserScan, self.callback, queue_size=5)
        rospy.loginfo("Started node invert_laser")

    def callback(self, data):
        actual_ranges = data.ranges[180:] + data.ranges[:180]
        data.ranges = actual_ranges
        data.header.frame_id = "inverted_laser"
        self.scan_publisher.publish(data)


if __name__ == '__main__':
    InvertLaser()
    rospy.spin()