#!/usr/bin/env python

import rospy
from l1_controller_v2 import L1ControllerV2
import math

FIXED_ANGLE_DEGREES = 22.0
FIXED_ANGLE_RADIANS = math.radians(FIXED_ANGLE_DEGREES)

LOWER_ETA_THRESHOLD = 30.0
LOWER_ETA_DEG2RAD  = math.radians(LOWER_ETA_THRESHOLD)
TANN_LOWER_ETA_THRESHOLD  = math.tan(LOWER_ETA_DEG2RAD)


class L1ControllerV3(L1ControllerV2):

    def __init__(self, node_name):
        super(L1ControllerV3, self).__init__(node_name)
        self.current_speed = self.steering_angle = 0

    def get_steering_and_speed(self, car_to_waypoint_vector):
        steering_angle = self.get_steering_angle(car_to_waypoint_vector)
        abs_steering = abs(steering_angle)
        if abs_steering > 0.26:
            # 15 degrees
            speed = 0.7
        elif abs_steering > 0.17:
            # 10 degrees
            speed = 0.9
        elif abs_steering > 0.09:
            # 5 degrees
            speed = 1.2
        else:
            speed = self.target_speed
        return speed, steering_angle

    def control_loop(self, timer_event):
        car_pose = self.current_pose

        if self.goal_received and not self.goal_reached:
            if self.current_path:
                car_to_waypoint_vector = self.get_car_to_waypoint_vector(car_pose)
                if self.found_target_waypoint:
                    if self.front_blocked and self.rear_blocked:
                        self.current_speed = self.steering_angle = 0
                        rospy.logwarn("Both front and rear blocked, cannot escape")
                    else:
                        if car_to_waypoint_vector.x > 0:
                            if self.front_blocked:
                                rospy.logwarn("Front blocked")
                                self.current_speed = -self.recovery_speed
                                if car_to_waypoint_vector.y > 0:
                                    self.steering_angle = -FIXED_ANGLE_RADIANS
                                else:
                                    self.steering_angle = FIXED_ANGLE_RADIANS
                            elif self.current_speed < -0.01:
                                # the car is moving backwards
                                if self.rear_blocked:
                                    rospy.logwarn("Rear blocked")
                                    self.current_speed, \
                                    self.steering_angle = self.get_steering_and_speed(car_to_waypoint_vector)
                                else:
                                    tan_eta = car_to_waypoint_vector.y / car_to_waypoint_vector.x
                                    if tan_eta > TANN_LOWER_ETA_THRESHOLD :
                                        self.steering_angle = -FIXED_ANGLE_RADIANS
                                        self.current_speed = -self.recovery_speed
                                    elif tan_eta < -TANN_LOWER_ETA_THRESHOLD :
                                        self.steering_angle = FIXED_ANGLE_RADIANS
                                        self.current_speed = -self.recovery_speed
                                    else:
                                        self.current_speed, \
                                        self.steering_angle = self.get_steering_and_speed(car_to_waypoint_vector)
                            else:
                                self.current_speed, \
                                self.steering_angle = self.get_steering_and_speed(car_to_waypoint_vector)
                        else:
                            # car_to_waypoint_vector.x < 0
                            if self.rear_blocked:
                                rospy.logwarn("Rear blocked")
                                self.current_speed = self.recovery_speed
                                if car_to_waypoint_vector.y > 0:
                                    self.steering_angle = FIXED_ANGLE_RADIANS
                                else:
                                    self.steering_angle = -FIXED_ANGLE_RADIANS
                            elif self.current_speed > 0.01:
                                if self.front_blocked:
                                    rospy.logwarn("Front blocked")
                                    self.current_speed = -self.recovery_speed
                                    if car_to_waypoint_vector.y > 0:
                                        self.steering_angle = -FIXED_ANGLE_RADIANS
                                    else:
                                        self.steering_angle = FIXED_ANGLE_RADIANS
                                else:
                                    self.current_speed = self.recovery_speed
                                    if car_to_waypoint_vector.y > 0:
                                        self.steering_angle = FIXED_ANGLE_RADIANS
                                    else:
                                        self.steering_angle = -FIXED_ANGLE_RADIANS
                            else:
                                self.current_speed = -self.recovery_speed
                                if car_to_waypoint_vector.y > 0:
                                    self.steering_angle = -FIXED_ANGLE_RADIANS
                                else:
                                    self.steering_angle = FIXED_ANGLE_RADIANS
                else:
                    self.current_speed = self.steering_angle = 0
                    rospy.logwarn("Target waypoint not found")
            else:
                self.current_speed = self.steering_angle = 0
                rospy.logwarn("Goal received but no path received yet")
        else:
            self.current_speed = self.steering_angle = 0

        self.publish_ackermann(self.steering_angle, self.current_speed)


if __name__ == '__main__':
    L1ControllerV3("l1_controller")
    rospy.spin()