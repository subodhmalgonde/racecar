#!/usr/bin/env python

"""
This node subscribes to the path sent by move_base global planner and it implements the pure pursuit
 algorithm and sends AckerMannDriveStamped messages
"""

import rospy
from nav_msgs.msg import Path, Odometry
from geometry_msgs.msg import PoseStamped, Point, PoseWithCovarianceStamped
from visualization_msgs.msg import Marker
from ackermann_publisher import AckermannPublisher
from std_msgs.msg import String
import math
import geometry_utils as Utils


class L1Controller(AckermannPublisher):

    def __init__(self, node_name):
        super(L1Controller, self).__init__(node_name)
        rospy.loginfo("Started L1 controller")

        rospy.Subscriber("/move_base/NavfnROS/plan", Path, self.path_callback, queue_size=5)
        rospy.Subscriber("/move_base_simple/goal", PoseStamped, self.goal_callback, queue_size=5)
        rospy.Subscriber("/pf/pose/odom", Odometry, self.odom_callback, queue_size=5)
        rospy.Subscriber("/amcl_pose", PoseWithCovarianceStamped, self.odom_callback, queue_size=5)

        self.marker_pub = rospy.Publisher("car_path", Marker, queue_size=5)
        self.goal_status_pub = rospy.Publisher("goal_status", String, queue_size=5)

        self.current_pose = None
        self.current_path = None
        self.current_goal = None
        self.goal_received = False
        self.goal_reached = False

        self.found_forward_waypoint = False
        self.forward_waypoint = None

        self.wheel_base = rospy.get_param("~wheel_base", 0.34)
        self.target_speed = rospy.get_param("~target_speed", 1.0)
        self.goal_tolerance = rospy.get_param("~goal_tolerance", 0.4)
        self.look_ahead_distance = rospy.get_param("~look_ahead_distance", 1.0)
        self.controller_frequency = rospy.get_param("~controller_frequency", 20)
        self.lfw = rospy.get_param("~lfw", 0.13)

        # self.look_ahead_distance = self.get_look_ahead_distance(self.target_speed)

        rospy.loginfo("[param] Goal tolerance: %f", self.goal_tolerance)
        rospy.loginfo("[param] Target speed: %.2f", self.target_speed)
        rospy.loginfo("[param] Look ahead distance: %.2f", self.look_ahead_distance)
        rospy.loginfo("[param] Wheel base: %.2f", self.wheel_base)
        rospy.loginfo("[param] lfw: %f", self.lfw)

        self.points = Marker()
        self.line_strip = Marker()
        self.goal_circle = Marker()

        self.initialize_markers()

        rospy.Timer(rospy.Duration(1.0/self.controller_frequency), self.control_loop)
        rospy.Timer(rospy.Duration(2.0/self.controller_frequency), self.check_if_goal_reached)

    def initialize_markers(self):
        self.points.header.frame_id = "map"
        self.line_strip.header.frame_id = "map"
        self.goal_circle.header.frame_id = "map"

        self.points.ns = self.line_strip.ns = self.goal_circle.ns = "Markers"
        self.points.action = self.line_strip.action = self.goal_circle.action = Marker.ADD

        self.points.id = 0
        self.line_strip.id = 1
        self.goal_circle.id = 2

        self.points.type = Marker.POINTS
        self.line_strip.type = Marker.LINE_STRIP
        self.goal_circle.type = Marker.CYLINDER

        # POINTS markers use x and y scale for width / height respectively
        self.points.scale.x = 0.2
        self.points.scale.y = 0.2

        # LINE_STRIP markers use only the x component of scale, for the line width
        self.line_strip.scale.x = 0.1

        self.goal_circle.scale.x = self.goal_tolerance
        self.goal_circle.scale.y = self.goal_tolerance
        self.goal_circle.scale.z = 0.1

        # Points are green
        self.points.color.g = 1.0
        self.points.color.a = 1.0

        # Line strip is blue
        self.line_strip.color.b = 1.0
        self.line_strip.color.a = 1.0

        # goal_circle is yellow
        self.goal_circle.color.r = 1.0
        self.goal_circle.color.g = 1.0
        self.goal_circle.color.b = 0.0
        self.goal_circle.color.a = 0.5

    def get_look_ahead_distance(self, target_speed):
        return 1.0

    def path_callback(self, msg):
        """
        :param msg: nav_msgs/Path
        - Header header
        - geometry_msgs/PoseStamped[] poses
        """
        self.current_path = msg.poses

    def goal_callback(self, msg):
        """
        :param msg: geometry_msgs/PoseStamped
        """
        self.goal_received = True
        self.goal_reached = False

        self.current_goal = msg.pose.position
        self.goal_circle.pose = msg.pose

        self.marker_pub.publish(self.goal_circle)

    def odom_callback(self, odom_msg):
        """
        :param odom_msg: nav_msgs/Odometry
        """
        self.current_pose = odom_msg.pose.pose

    def is_waypoint_ahead(self, way_point, car_position, car_yaw):
        """
        :param way_point: geometry_msgs/Point
        :param car_position: geometry_msgs/Point
        :param car_yaw: yaw in radians
        :return: Boolean. True if the waypoint is ahead of the car in the car's coordinate system
        """
        dx = way_point.x - car_position.x
        dy = way_point.y - car_position.y

        car_car2wayPt_x = math.cos(car_yaw) * dx + math.sin(car_yaw) * dy
        # car_car2wayPt_y = -math.sin(car_yaw) * dx + math.cos(car_yaw) * dy

        if car_car2wayPt_x > 0:
            return True
        else:
            return False

    def is_waypoint_farther_than_look_ahead(self, waypoint, car_position):
        """
        :param waypoint: geometry_msgs/Point
        :param car_position: geometry_msgs/Point
        :return: True if the distance between car and waypoint is greater than the look ahead distance
        """
        dx = waypoint.x - car_position.x
        dy = waypoint.y - car_position.y
        dist = math.sqrt(dx*dx + dy*dy)

        return dist >= self.look_ahead_distance

    def get_car_to_waypoint_vector(self, car_pose):
        car_position = car_pose.position
        car_yaw = Utils.quaternion_to_yaw(car_pose.orientation)

        self.found_forward_waypoint = False
        self.forward_waypoint = None

        if not self.goal_reached:
            for pose_stamped in self.current_path:
                waypoint = pose_stamped.pose.position
                _is_forward_waypoint = self.is_waypoint_ahead(waypoint, car_position, car_yaw)
                if _is_forward_waypoint:
                    if self.is_waypoint_farther_than_look_ahead(waypoint, car_position):
                        self.found_forward_waypoint = True
                        self.forward_waypoint = waypoint
                        rospy.loginfo("Nearest waypoint (x, y) = %.2f, %.2f", waypoint.x, waypoint.y)
                        break
            if not self.found_forward_waypoint:
                self.forward_waypoint = self.current_path[-1].pose.position
                self.found_forward_waypoint = True
                rospy.logwarn("Nearest waypoint closer than look ahead distance. Choosing last waypoint.")
                rospy.loginfo("Nearest waypoint (x, y) = %.2f, %.2f", self.forward_waypoint.x, self.forward_waypoint.y)
        else:
            # Goal has been reached
            self.found_forward_waypoint = False
            self.forward_waypoint = self.current_goal

        del self.points.points[:]
        del self.line_strip.points[:]

        if self.found_forward_waypoint and not self.goal_reached:
            self.points.points.append(car_position)
            self.points.points.append(self.forward_waypoint)
            self.line_strip.points.append(car_position)
            self.line_strip.points.append(self.forward_waypoint)

        self.marker_pub.publish(self.points)
        self.marker_pub.publish(self.line_strip)

        dx = self.forward_waypoint.x - car_position.x
        dy = self.forward_waypoint.y - car_position.y

        car_to_waypoint_vector = Point()
        car_to_waypoint_vector.x = math.cos(car_yaw)*dx + math.sin(car_yaw)*dy
        car_to_waypoint_vector.y = -math.sin(car_yaw)*dx + math.cos(car_yaw)*dy

        rospy.loginfo("Car to waypoint vector (x, y) = %.2f, %.2f", car_to_waypoint_vector.x, car_to_waypoint_vector.y)

        return car_to_waypoint_vector

    def get_eta(self, car_pose):
        car_to_waypoint_vector = self.get_car_to_waypoint_vector(car_pose)
        eta = math.atan2(car_to_waypoint_vector.y, car_to_waypoint_vector.x)
        rospy.loginfo("Eta %.2f", eta)
        return eta

    def get_car_to_goal_distance(self):
        car_position = self.current_pose.position
        dx = self.current_goal.x - car_position.x
        dy = self.current_goal.y - car_position.y

        return math.sqrt(dx*dx + dy*dy)

    def get_steering_angle(self, eta):
        steering_angle = math.atan2(self.wheel_base * math.sin(eta), self.look_ahead_distance/3.0 + self.lfw * math.cos(eta))
        rospy.loginfo("Steering Angle (degrees) = %.2f", steering_angle * (180.0 / math.pi))
        return steering_angle

    def check_if_goal_reached(self, timer_event):
        if self.goal_received and (self.get_car_to_goal_distance() <= self.goal_tolerance):
            self.goal_received = False
            self.goal_reached = True
            rospy.loginfo("Goal reached!")
            self.goal_status_pub.publish("next")

    def control_loop(self, timer_event):
        car_pose = self.current_pose
        speed = steering_angle = 0

        if self.goal_received:
            if self.current_path:
                eta = self.get_eta(car_pose)
                if self.found_forward_waypoint:
                    steering_angle = self.get_steering_angle(eta)
                    speed = self.target_speed
            else:
                rospy.logwarn("Goal received but no path received yet")

        self.publish_ackermann(steering_angle, speed)


if __name__ == '__main__':
    L1Controller("l1_controller")
    rospy.spin()