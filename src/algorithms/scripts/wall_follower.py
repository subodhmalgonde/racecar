#!/usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan
from ackermann_msgs.msg import AckermannDriveStamped
from ackermann_publisher import AckermannPublisher
import math
import numpy as np

FIXED_ANGLE_DEGREES = 30
FIXED_ANGLE_RADIANS = math.radians(FIXED_ANGLE_DEGREES)

COS_FIXED_ANGLE = math.cos(FIXED_ANGLE_RADIANS)
SIN_FIXED_ANGLE = math.sin(FIXED_ANGLE_RADIANS)

class WallFollowerException(Exception):
    pass

class DistanceFinderNode(AckermannPublisher):
    def __init__(self, node_name):
        super(DistanceFinderNode, self).__init__(node_name)

        self.target_speed = rospy.get_param("~target_speed", 0.5)
        self.target_distance = rospy.get_param("~target_distance", 0.5)

        if not (rospy.has_param("~kp") and rospy.has_param("~kd") and rospy.has_param("~ki")):
            raise WallFollowerException("PID parameters not set in the parameter server")

        self.kp = rospy.get_param("~kp")
        self.kd = rospy.get_param("~kd")
        self.ki = rospy.get_param("~ki")
        self.previous_error = 0
        self.cumultative_error = 0

        # parameter for specifying if the LIDAR is kept in an inverted position
        self.inverted_lidar = rospy.get_param("~inverted_lidar", False)

        rospy.Subscriber(rospy.get_param("~scan_topic", "scan"), LaserScan, self.callback)
        self.first_scan_received = False

        # set default values for the scanning parameters of the LIDAR
        # these are for the RPLidar
        self.angle_min = -(math.pi - math.radians(1)) #179 degrees
        self.angle_max = math.radians(180)  # 180 degrees
        self.angle_increment = math.radians(1) # 1 degree
        self.len_ranges = 360

    def callback(self, message):
        if not self.first_scan_received:
            # rospy.loginfo("First scan received")
            self.first_scan_received = True
            self.angle_min = message.angle_min
            self.angle_max = message.angle_max
            self.angle_increment = message.angle_increment
            self.len_ranges = len(message.ranges)

            # rospy.loginfo("angle min: %.1f, angle max: %.1f, angle increment: %.1f" \
            #               % (math.degrees(self.angle_min), math.degrees(self.angle_max),
            #                  math.degrees(self.angle_increment))
            #               )

            # rospy.loginfo("Number of range measurements %d" % self.len_ranges)

        # calculate distance from wall and publish to /pid_error

        if self.inverted_lidar:
            # The laser is positioned in the reverse direction, so some changes are required
            # actual_ranges = message.ranges[180:] + message.ranges[:180]
            # right_ranges = actual_ranges[:180], in other words
            right_ranges = message.ranges[180:]

            # left_ranges = actual_ranges[180:], in other words
            # left_ranges = message.ranges[:180]
        else:
            right_ranges = message.ranges[:180]
            # left_ranges = message.ranges[180:]

        # equations taken from F1/10 tutorials

        # range perpendicular to the car
        range_at_zero = right_ranges[89]

        # range at an angle of 60 degrees
        range_at_fixed_angle = right_ranges[89+FIXED_ANGLE_DEGREES]

        # rospy.loginfo("Range at zero %.2f, range at thirty %.2f" % (range_at_zero, range_at_fixed_angle))

        # the orientation wrt to the center line
        orientation = math.atan2(range_at_fixed_angle*COS_FIXED_ANGLE - range_at_zero,
                                 range_at_fixed_angle*SIN_FIXED_ANGLE)

        current_distance = range_at_zero*math.cos(orientation)

        # assume there is a delay in actuation and propagate the distance into the future
        future_distance = current_distance + self.target_speed*0.5*math.sin(orientation)
        error = self.target_distance - future_distance
        # rospy.loginfo("Distance from wall: current %.2f, future %.2f" % (current_distance, future_distance))

        self.compute_and_apply_control(error)

    def compute_and_apply_control(self, error):
        steering = self.kp*error + self.kd*(error-self.previous_error) + self.ki*self.cumultative_error
        # rospy.loginfo("Computed steering: %.2f" % steering)

        self.previous_error = error
        self.cumultative_error += error

        # limit the steering angle to +/- 30 degrees
        steering = np.clip(steering, -FIXED_ANGLE_RADIANS, FIXED_ANGLE_RADIANS)
        speed = self.target_speed
        if abs(steering) > math.radians(20):
            speed = 0.6
        elif abs(steering) > math.radians(10):
            speed = 0.8

        # rospy.loginfo("Clipped steering: %.2f, target speed %.2f" % (math.degrees(steering), speed))

        self.publish_ackermann(steering, speed)


if __name__ == '__main__':
    DistanceFinderNode("dist_finder")
    rospy.spin()