#!/usr/bin/env python

"""
This node subscribes to the path sent by move_base global planner and it implements the pure pursuit
 algorithm and sends AckerMannDriveStamped messages
"""

import rospy
from nav_msgs.msg import Path, Odometry
from geometry_msgs.msg import PoseStamped, Point, PoseWithCovarianceStamped
from visualization_msgs.msg import Marker
from ackermann_publisher import AckermannPublisher
from std_msgs.msg import String, Int16
import math
import geometry_utils as Utils

FIXED_ANGLE_DEGREES = 22.0
FIXED_ANGLE_RADIANS = math.radians(FIXED_ANGLE_DEGREES)

UPPER_ETA_THRESHOLD = 40.0
UPPER_ETA_DEG2RAD = math.radians(UPPER_ETA_THRESHOLD)
TAN_UPPER_ETA_THRESHOLD = math.tan(UPPER_ETA_DEG2RAD)

LOWER_ETA_THRESHOLD = 25.0
LOWER_ETA_DEG2RAD  = math.radians(LOWER_ETA_THRESHOLD)
TANN_LOWER_ETA_THRESHOLD  = math.tan(LOWER_ETA_DEG2RAD)

class L1ControllerV2(AckermannPublisher):

    def __init__(self, node_name):
        super(L1ControllerV2, self).__init__(node_name)
        rospy.loginfo("Started L1 controller")

        rospy.Subscriber("/move_base/NavfnROS/plan", Path, self.path_callback, queue_size=5)
        rospy.Subscriber("/move_base_simple/goal", PoseStamped, self.goal_callback, queue_size=5)
        rospy.Subscriber("/pf/pose/odom", Odometry, self.odom_callback, queue_size=5)
        rospy.Subscriber("/amcl_pose", PoseWithCovarianceStamped, self.odom_callback, queue_size=5)
        rospy.Subscriber("/obstacle_detected", Int16, self.obstacle_callback, queue_size=3)

        self.marker_pub = rospy.Publisher("car_path", Marker, queue_size=5)
        self.goal_status_pub = rospy.Publisher("goal_status", String, queue_size=5)

        self.front_blocked = False
        self.rear_blocked = False

        self.current_pose = None
        self.current_path = None
        self.current_goal = None
        self.goal_received = False
        self.goal_reached = False

        self.found_target_waypoint = False
        self.target_waypoint = None

        self.wheel_base = rospy.get_param("~wheel_base", 0.34)
        self.target_speed = rospy.get_param("~target_speed", 1.0)
        self.goal_tolerance = rospy.get_param("~goal_tolerance", 0.4)
        self.look_ahead_distance = rospy.get_param("~look_ahead_distance", 1.0)
        self.controller_frequency = rospy.get_param("~controller_frequency", 20)
        self.lfw = rospy.get_param("~lfw", 0.13)
        self.recovery_speed = rospy.get_param("~recovery_speed", 0.6)

        # self.look_ahead_distance = self.get_look_ahead_distance(self.target_speed)

        rospy.loginfo("[param] Goal tolerance: %f", self.goal_tolerance)
        rospy.loginfo("[param] Target speed: %.2f", self.target_speed)
        rospy.loginfo("[param] Look ahead distance: %.2f", self.look_ahead_distance)
        rospy.loginfo("[param] Wheel base: %.2f", self.wheel_base)
        rospy.loginfo("[param] lfw: %f", self.lfw)
        rospy.loginfo("[param] recovery_speed: %f", self.recovery_speed)

        self.move_base_stop_publisher = rospy.Publisher("/move_base_simple/stop", String, queue_size=1)

        self.points = Marker()
        self.line_strip = Marker()
        self.goal_circle = Marker()

        self.initialize_markers()

    def initialize_markers(self):
        self.points.header.frame_id = "map"
        self.line_strip.header.frame_id = "map"
        self.goal_circle.header.frame_id = "map"

        self.points.ns = self.line_strip.ns = self.goal_circle.ns = "Markers"
        self.points.action = self.line_strip.action = self.goal_circle.action = Marker.ADD

        self.points.id = 0
        self.line_strip.id = 1
        self.goal_circle.id = 2

        self.points.type = Marker.POINTS
        self.line_strip.type = Marker.LINE_STRIP
        self.goal_circle.type = Marker.CYLINDER

        # POINTS markers use x and y scale for width / height respectively
        self.points.scale.x = 0.2
        self.points.scale.y = 0.2

        # LINE_STRIP markers use only the x component of scale, for the line width
        self.line_strip.scale.x = 0.1

        self.goal_circle.scale.x = self.goal_tolerance
        self.goal_circle.scale.y = self.goal_tolerance
        self.goal_circle.scale.z = 0.1

        # Points are green
        self.points.color.g = 1.0
        self.points.color.a = 1.0

        # Line strip is blue
        self.line_strip.color.b = 1.0
        self.line_strip.color.a = 1.0

        # goal_circle is yellow
        self.goal_circle.color.r = 1.0
        self.goal_circle.color.g = 1.0
        self.goal_circle.color.b = 0.0
        self.goal_circle.color.a = 0.5

    def get_look_ahead_distance(self, target_speed):
        return 1.0

    def path_callback(self, msg):
        """
        :param msg: nav_msgs/Path
        - Header header
        - geometry_msgs/PoseStamped[] poses
        """
        self.current_path = msg.poses

    def goal_callback(self, msg):
        """
        :param msg: geometry_msgs/PoseStamped
        """
        self.goal_received = True
        self.goal_reached = False

        self.current_goal = msg.pose.position
        self.goal_circle.pose = msg.pose

        self.marker_pub.publish(self.goal_circle)

        self.goal_timer = rospy.Timer(rospy.Duration(2.0 / self.controller_frequency), self.check_if_goal_reached)
        self.controller_timer = rospy.Timer(rospy.Duration(1.0 / self.controller_frequency), self.control_loop)

    def odom_callback(self, odom_msg):
        """
        :param odom_msg: nav_msgs/Odometry
        """
        self.current_pose = odom_msg.pose.pose

    def get_waypoint_in_car_coordinates(self, way_point, car_position, cos_car_yaw, sin_car_yaw):
        """
        :param way_point: geometry_msgs/Point
        :param car_position: geometry_msgs/Point
        :param car_yaw: yaw in radians
        :return: Boolean. True if the waypoint is ahead of the car in the car's coordinate system
        """
        dx = way_point.x - car_position.x
        dy = way_point.y - car_position.y

        car_car2wayPt_x = cos_car_yaw * dx + sin_car_yaw * dy
        car_car2wayPt_y = -sin_car_yaw * dx + cos_car_yaw * dy

        return car_car2wayPt_x, car_car2wayPt_y

    def get_car_to_waypoint_vector(self, car_pose):
        car_position = car_pose.position
        car_yaw = Utils.quaternion_to_yaw(car_pose.orientation)
        cos_car_yaw = math.cos(car_yaw)
        sin_car_yaw = math.sin(car_yaw)

        self.found_target_waypoint = False
        self.target_waypoint = None

        waypt_car_x = waypt_car_y = None

        if not self.goal_reached:
            for pose_stamped in self.current_path:
                waypoint = pose_stamped.pose.position
                waypt_car_x, waypt_car_y = self.get_waypoint_in_car_coordinates(
                    waypoint, car_position, cos_car_yaw, sin_car_yaw
                )
                waypt_car_dist  = math.sqrt(waypt_car_x*waypt_car_x + waypt_car_y*waypt_car_y)
                if waypt_car_dist >= self.look_ahead_distance:
                    self.found_target_waypoint = True
                    self.target_waypoint = waypoint
                    #rospy.loginfo("Nearest waypoint (x, y) = %.2f, %.2f", waypoint.x, waypoint.y)
                    break
            if not self.found_target_waypoint:
                self.target_waypoint = self.current_path[-1].pose.position
                self.found_target_waypoint = True
                # rospy.logwarn("Nearest waypoint closer than look ahead distance. Choosing last waypoint.")
                # rospy.loginfo("Nearest waypoint (x, y) = %.2f, %.2f", self.target_waypoint.x, self.target_waypoint.y)
        else:
            # Goal has been reached
            self.found_target_waypoint = False
            self.target_waypoint = self.current_goal

        del self.points.points[:]
        del self.line_strip.points[:]

        if self.found_target_waypoint and not self.goal_reached:
            self.points.points.append(car_position)
            self.points.points.append(self.target_waypoint)
            self.line_strip.points.append(car_position)
            self.line_strip.points.append(self.target_waypoint)

        self.marker_pub.publish(self.points)
        self.marker_pub.publish(self.line_strip)

        car_to_waypoint_vector = Point()
        car_to_waypoint_vector.x = waypt_car_x
        car_to_waypoint_vector.y = waypt_car_y

        #rospy.loginfo("Car to waypoint vector (x, y) = %.2f, %.2f", car_to_waypoint_vector.x, car_to_waypoint_vector.y)

        return car_to_waypoint_vector

    def get_eta(self, car_to_waypoint_vector):
        eta = math.atan2(car_to_waypoint_vector.y, car_to_waypoint_vector.x)
        #rospy.loginfo("Eta %.2f", eta)
        return eta

    def get_car_to_goal_distance(self):
        car_position = self.current_pose.position
        dx = self.current_goal.x - car_position.x
        dy = self.current_goal.y - car_position.y

        return math.sqrt(dx*dx + dy*dy)

    def get_steering_angle(self, car_to_waypoint_vector):
        eta = self.get_eta(car_to_waypoint_vector)
        steering_angle = math.atan2(self.wheel_base * math.sin(eta),
                                    self.look_ahead_distance*2.0/3.0 + self.lfw * math.cos(eta))
        # rospy.loginfo("Steering Angle (degrees) = %.2f", steering_angle * (180.0 / math.pi))
        return steering_angle

    def check_if_goal_reached(self, timer_event):
        if self.goal_received and (self.get_car_to_goal_distance() <= self.goal_tolerance):
            # stop the global planner
            stop_action = String()
            stop_action.data = "stop"
            self.move_base_stop_publisher.publish(stop_action)

            self.controller_timer.shutdown()
            self.goal_timer.shutdown()

            self.current_speed = self.steering_angle = 0

            self.goal_received = False
            self.goal_reached = True
            self.current_path = None

            rospy.loginfo("Goal reached!")

            self.goal_status_pub.publish("next")

    def control_loop(self, timer_event):
        car_pose = self.current_pose

        if self.goal_received and not self.goal_reached:
            if self.current_path:
                car_to_waypoint_vector = self.get_car_to_waypoint_vector(car_pose)
                if self.found_target_waypoint:
                    if self.front_blocked and self.rear_blocked:
                        self.current_speed = self.steering_angle = 0
                        rospy.logwarn("Both front and rear blocked, cannot escape")
                    else:
                        if self.front_blocked:
                            rospy.logwarn("Front blocked")
                            self.current_speed = -1 * self.target_speed
                            if car_to_waypoint_vector.y < 0:
                                self.steering_angle = FIXED_ANGLE_RADIANS
                            else:
                                self.steering_angle = -1*FIXED_ANGLE_RADIANS
                        elif self.rear_blocked:
                            rospy.logwarn("Rear blocked")
                            self.current_speed = self.target_speed
                            if car_to_waypoint_vector.x > 0:
                                self.steering_angle = self.get_steering_angle(car_to_waypoint_vector)
                            else:
                                if car_to_waypoint_vector.y < 0:
                                    self.steering_angle = -1*FIXED_ANGLE_RADIANS
                                else:
                                    self.steering_angle = FIXED_ANGLE_RADIANS
                        else:
                            if car_to_waypoint_vector.x > 0:
                                tan_eta = car_to_waypoint_vector.y/car_to_waypoint_vector.x

                                if self.current_speed < -0.01:
                                    # if the car is moving backwards

                                    # if the waypoint is too far to the right or left
                                    # Take a reverse turn
                                    if tan_eta > TANN_LOWER_ETA_THRESHOLD :
                                        self.steering_angle = -1 * FIXED_ANGLE_RADIANS
                                        self.current_speed = -1 * self.target_speed
                                    elif tan_eta < -TANN_LOWER_ETA_THRESHOLD :
                                        self.steering_angle = FIXED_ANGLE_RADIANS
                                        self.current_speed = -1 * self.target_speed
                                    else:
                                        self.current_speed = self.target_speed
                                        self.steering_angle = self.get_steering_angle(car_to_waypoint_vector)
                                else:
                                    # if the waypoint is too far to the right or left
                                    # Take a reverse turn
                                    if tan_eta > TAN_UPPER_ETA_THRESHOLD:
                                        self.steering_angle = -1 * FIXED_ANGLE_RADIANS
                                        self.current_speed = -1 * self.target_speed
                                    elif tan_eta < -TAN_UPPER_ETA_THRESHOLD:
                                        self.steering_angle = FIXED_ANGLE_RADIANS
                                        self.current_speed = -1 * self.target_speed
                                    else:
                                        self.current_speed = self.target_speed
                                        self.steering_angle = self.get_steering_angle(car_to_waypoint_vector)
                            else:
                                self.current_speed = -1*self.target_speed
                                if car_to_waypoint_vector.y < 0:
                                    self.steering_angle = FIXED_ANGLE_RADIANS
                                else:
                                    self.steering_angle = -1*FIXED_ANGLE_RADIANS
                else:
                    self.current_speed = self.steering_angle = 0
                    rospy.logwarn("Target waypoint not found")
            else:
                self.current_speed = self.steering_angle = 0
                rospy.logwarn("Goal received but no path received yet")
        else:
            self.current_speed = self.steering_angle = 0

        self.publish_ackermann(self.steering_angle, self.current_speed)

    def obstacle_callback(self, msg):
        # 0: No obstacles closeby
        # 1: Front blocked
        # 2: Back blocked
        # 3: Front & back blocked
        code = msg.data
        if code == 0:
            self.front_blocked = False
            self.rear_blocked = False
        elif code == 1:
            self.front_blocked = True
            self.rear_blocked = False
        elif code == 2:
            self.front_blocked = False
            self.rear_blocked = True
        elif code == 3:
            self.front_blocked = True
            self.rear_blocked = True

if __name__ == '__main__':
    L1ControllerV2("l1_controller")
    rospy.spin()