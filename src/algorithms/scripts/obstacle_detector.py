#!/usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Int16
import numpy as np

"""
Base class for nodes which publish the AckermannDriveStamped message
"""


class ObstacleDetector(object):

    def __init__(self):
        rospy.init_node("obstacle_detector", log_level=rospy.INFO)

        # queue size = 1 so that you check the latest scan always
        rospy.Subscriber('/scan', LaserScan, self.scan_cb, queue_size=1)

        # 0: No obstacles closeby
        # 1: Front blocked
        # 2: Back blocked
        # 3: Front & back blocked
        self.publisher = rospy.Publisher('/obstacle_detected', Int16, queue_size=3)

        self.front_threshold = rospy.get_param("~front_threshold", 0.40)
        self.rear_threshold = rospy.get_param("~rear_threshold", 0.45)
        rospy.loginfo("Started node obstacle_detector")

    def scan_cb(self, msg):
        back_ranges = np.array(msg.ranges[:25] + msg.ranges[334:])
        front_ranges = np.array(msg.ranges[155:205])

        front_blocked = np.sum(front_ranges < self.front_threshold) > 5
        back_blocked = np.sum(back_ranges < self.rear_threshold) > 5

        blocked_code = 0
        if front_blocked & back_blocked:
            blocked_code = 3
        elif front_blocked:
            blocked_code = 1
        elif back_blocked:
            blocked_code = 2

        msg = Int16()
        msg.data = blocked_code

        self.publisher.publish(blocked_code)


if __name__ == "__main__":
    ObstacleDetector()
    rospy.spin()