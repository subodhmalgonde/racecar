import tf.transformations
import tf
import numpy


def quaternion_to_yaw(q):
    """Convert a Quaternion into yaw in radians."""
    x, y, z, w = q.x, q.y, q.z, q.w
    roll, pitch, yaw = tf.transformations.euler_from_quaternion((x, y, z, w))
    return yaw
