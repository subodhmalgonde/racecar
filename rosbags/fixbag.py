import os
import sys
import rosbag

def print_usage():
    print("Usage: fixbag.py <ros bag>")
    print("Example: fixbag.py recorded.bag")


if __name__ == "__main__":
    # Check CLI args
    if len(sys.argv) != 2:
        print_usage()
        exit(-1)

    bag_path = sys.argv[1]  # Path to ROS bag you want to repair
    repair_path = bag_path.replace(".bag", "-repaired.bag")  # Output bag path

    with rosbag.Bag(repair_path, 'w') as outbag:
        for topic, msg, t in rosbag.Bag(bag_path).read_messages():

            """
            Approach 1 (use any 1): Replace bag timestamps with message header timestamps 
            """

            # This also replaces tf timestamps under the assumption
            # that all transforms in the message share the same timestamp
            if topic == "/tf" and msg.transforms:
                outbag.write(topic, msg, msg.transforms[0].header.stamp)
            else:
                outbag.write(topic, msg, msg.header.stamp if msg._has_header else t)

            """
            Approach 2 (use any 1): Replace message header timestamps with bag timestamps  
            """
            # set all message timestamps to the rosbag timestamps
            # i.e. this will set all timestamps to the machine where rosbag was recorded
            # msg.header.stamp.secs = t.secs
            # msg.header.stamp.nsecs = t.nsecs
            # outbag.write(topic, msg, t)